This library is a Python client using Castiphone api for Onaro project.
More info about Castiphone api https://bezeroak.onaro.eus/api/.

## Resources

* Cliente - Client api
* Login - Login api

## Installation

```commandline
$ pip install pycastiphone-client
```

## Configuration Environment

You need define the CASTIPHONE_CLIENT_ID as environment variables. You need define:

```
CASTIPHONE_CLIENT_ID=<client_credentials>
```

If this envvars are not defined, a exception will be raised with the name of the envvar not defined.

## Usage

To use this client in Onaro project.

#### Create a Login

```
json

{
    "access_token": "********",
    "token_type": "bearer",
    "expires_in": 86399
}

```

#### Create a Cliente

```
json

{
    "codigo": 41,
    "razonSocial": "sample string 2",
    "nombre": null,
    "direccion": null,
    "poblacion": null,
    "provincia": null,
    "cPostal": null,
    "telefono": null,
    "movil": null,
    "fax": null,
    "correo": null,
    "banco": null,
    "cuentaBanco": null,
    "bic": null,
    "mandatoSepa": "41",
    "tipoRemesa": 0,
    "fechaMandato": "2021-07-30T19:20:56.5846284+02:00",
    "primerMandato": 0,
    "idVendedor": 0,
    "vencimiento": 0,
    "iva": 21.0,
    "cuentaContable": null,
    "comentarios": null,
    "tipoCliente": 0,
    "formaPago": 3,
    "idioma": 0,
    "observaciones": null,
    "fechaAlta": "2021-07-30T19:20:56.2412023+02:00",
    "facturar": null,
    "productos": [],
    "telefonos": []
}

```


