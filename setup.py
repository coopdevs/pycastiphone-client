# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


VERSION = "0.1.0"

setup(
    name="pycastiphone-client",
    version=VERSION,
    author="Coopdevs",
    author_email="info@coopdevs.org",
    maintainer="Konykon",
    url="https://gitlab.com/coopdevs/pycastiphone-client",
    description="Python wrapper for Castiphone (using REST API)",
    long_description="README",
    long_description_content_type="text/markdown",
    packages=find_packages(exclude=("tests", "docs")),
    include_package_data=True,
    zip_safe=False,
    install_requires=["requests"],
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        "Development Status :: 3 - Alpha",
        "Operating System :: POSIX :: Linux",
        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        "Programming Language :: Python :: 3.8.3"
    ],
)
