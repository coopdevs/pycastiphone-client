import pytest


def scrub_response(response):
    response["headers"]["Cookie"] = "****"
    response["headers"]["Set-Cookie"] = "****"
    if response["status"]["code"] == 200:
        response["body"]["string"] = '{"access_token":"****","token_type":"bearer","expires_in":86399}'
    return response

def scrub_request(request):
    if request.path == '/api/api/login':
        request.body = 'grant_type=client_credentials&client_id=****'
    return request

@pytest.fixture(scope="module")
def vcr_config():
    return {
        "filter_headers": [("Authorization", "****")],
        "before_record_request": scrub_request,
        "before_record_response": scrub_response,
    }
